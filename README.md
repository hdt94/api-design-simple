# API design

Minimal API design exercise considering as general use case buying tickets that can be sold out. This is a continuation of previous ER diagram design exercise.

API source: [openapi.yaml](openapi.yaml); use a visualizer such as GitLab built-in OpenAPI visualizer.

OpenAPI generated docs at [hdt94.gitlab.io/api-design-simple](https://hdt94.gitlab.io/api-design-simple)

Table of content:
- [Design](#design)
- [API Summary](#api-summary)
- [References](#references)

## Design

Entities:
- events: market events.
- locations: locations in city.
- places: marked places within locations to be accessed through ticket purchase.
- prices: event prices based on category.
- purchases: ticket purchases by users.
- tickets: ticket for a place in an event.
- users: system users that buy tickets.

Use cases:
- User checks events in a location.
- User checks prices and available places in an event location
- User handles auth (i.e. login and logout).
- User buys a ticket for a selected place in an event location.

Auth requests and paths:
- `POST /login`
- `POST /logout`

General path of `COLLECTION` requests, considering `COLLECTION` as a plural reference of any mentioned entity:
- `/COLLECTION`: read many resources or create a new resource
- `/COLLECTION/ID`: read, update, or delete a resource; where `ID` is an identifier.

Where auth credentials are required by default for all requests and paths, except for public read access of `GET /COLLECTION` and `GET /COLLECTION/ID` for `locations`, `events`, `places`, `prices`, and `tickets` (i.e. read access for `/purchases` and `/users` still requires auth credentials).

Regarding responses, consider following two main access patterns:
- Accessing individual item based on identifier:
    - General schema of response body:
        ```json
        {
            "item": RESOURCE_SCHEMA,
            "links": [LINK_SCHEMA]
        }
        ```
- Accessing multiple items based on paging and searching:
    - General schema of response body:
        ```json
        {
            "items": [RESOURCE_SCHEMA],
            "links": [LINK_SCHEMA]
        }
        ```
    - Generic querying parameters: `page`, `pageSize`, `search`

Where:
- "links" field enables Hypermedia Controls.
- events, locations, prices, and tickets resources collections have custom parameters, detaily explained at [openapi.yaml](openapi.yaml)

## API Summary

A detailed API description is available in OpenAPI generated docs published at [hdt94.gitlab.io/api-design-simple](https://hdt94.gitlab.io/api-design-simple) serving [public/index.html](public/index.html). You can also check source [openapi.yaml](openapi.yaml) along with manifests at [components/](components/)

Generate API docs into [public/index.html](public/index.html):
```shell
openapi-generator generate -i openapi.yaml -g html2 -o public/
```

Note: API should support "application/x-www-form-urlencoded" MIME type for form submitting, however, openapi-generator tool does not support auto generation of such MIME type.

API source: [openapi.yaml](openapi.yaml); use a visualizer such as GitLab built-in OpenAPI visualizer.

API text summary:
```
API METHODS - AUTH
login
logout

API METHODS - EVENTS
createEvent
deleteEvent
readEvent
readEvents
updateEvent

API METHODS - LOCATIONS
createLocation
deleteLocation
readLocation
readLocations
updateLocation

API METHODS - PLACES
createPlace
deletePlace
readPlace
readPlaces
updatePlace

API METHODS - PRICES
createPrice
deletePrice
readPrice
readPrices
updatePrice

API METHODS - PURCHASES
createPurchase
deletePurchase
readPurchase
readPurchases
updatePurchase

API METHODS - TICKETS
createTicket
deleteTicket
readTicket
readTickets
updateTicket

API METHODS - USERS
createUser
deleteUser
login
logout
readUser
updateUser
```

## References
- OpenAPI Specification: https://spec.openapis.org/oas/latest.html
- Swagger OpenAPI guide: https://swagger.io/docs/specification/about
- Article: How to create a really great RESTful API and why is it still a key to building a stable architecture? https://yalantis.com/blog/how-to-create-a-restful-api/
- Article: Richardson Maturity Model: https://martinfowler.com/articles/richardsonMaturityModel.html
- https://cloud.google.com/apis/design